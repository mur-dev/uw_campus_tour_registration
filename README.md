# UW Campus Tour Registration Module
This module allows a user to fill out a form where they can register a tour for a campus, faculty, and college tour. Their data is stored in the CRM and a confirmation email is sent to them.

[uwaterloo.ca/future-students/visit-waterloo/book-campus-tour](https://uwaterloo.ca/future-students/visit-waterloo/book-campus-tour)

[uwaterloo.ca/future-students/admin/config/book-campus-tour](https://uwaterloo.ca/future-students/admin/config/book-campus-tour)

[uwaterloo.ca/future-students/visit-waterloo/book-group-tour](https://uwaterloo.ca/future-students/visit-waterloo/book-group-tour)

[uwaterloo.ca/future-students/admin/config/book-group-tour](https://uwaterloo.ca/future-students/admin/config/book-group-tour)

---
---
# Functions

---
---
#### function uw_campus_tour_registration_menu()
This hook enables the module to register paths in order to define how URL requests are handled.

##### Returns
An associative array whose keys define paths and whose values are an associative array of properties for each path.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7.x) for this specific hook for more information.

---
---
#### function uw_campus_tour_permissions($roles = array())
This gives custom access based on what the role permissions are.

##### Returns
If the user is an admin, it will return true and give them permission to view and edit the page. It will return false otherwise.

---
---
#### function uw_campus_tour_admin()
Allows for the configuration of this module by a site manager. These are the fields that can be modified in CKEditor in drupal.

##### Returns
The fields that are editable by a site manager.

---
---
### function uw_group_tour_admin()
Allows for the configuration of this module by a site manager. These are the fields that can be modified in CKEditor in drupal.

##### Returns
The fields that are editable by a site manager.

---
---
### function uw_campus_tour_registration_get_promo_notice()
Retrieves the top blurb of the form.

---
---
### function uw_campus_tour_registration_schedule_generator()
Generates a table based on the date that was selected by the user to show which tours are available on which days.

##### Returns
The html code of the generated table.

---
---
### function uw_campus_tour_registration_redate_validate($form, &$form_state)
Checks to see which redate button was pressed on and changes the dates of the tours accordingly. This is done before the page refreshes.

---
---
### function uw_campus_tour_registration_form_0($form, &$form_state)
### function uw_campus_tour_registration_form_1($form, &$form_state)
### function uw_campus_tour_registration_form_2($form, &$form_state)
This is the actual form of the page that users will see. It is comprised of various types, including, but not limited to:
- button
- checkbox
- markup
- select
- submit
- textarea
- textfield

This form is paginated. The user must go through 3 pages in order to complete the form. In the first page, the user must select a date. This will generate the second page, where the user can see which tours are available on which days. They must pick a day. This will generate the third page, where they can add which tours they would like to see. Once completed, a confirmation page will be generated and a confirmation email will be sent to them.

##### Returns
The form that is rendered and viewable by the user.

##### Additional Information
See the [form api documentation](https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x) for the various types, their descriptions, their properties, and their examples.

---
---
### function uw_group_tour_registration_generate_year()
Generates a list of valid years for the entry year field.

##### Returns
An array of valid years.

---
---
### function uw_group_tour_registration_form($form_state)
This is the actual form of the page that users will see. It is comprised of various types, including, but not limited to:
- button
- checkbox
- markup
- select
- submit
- textarea
- textfield

This form is for the group tour page.

##### Returns
The form that is rendered and viewable by the user.

---
---
### function uw_campus_tour_registration_date_validate($form, &$form_state)
Validates the input of the user. If there is an issue, the form will reset and an error will appear indicating the issue to the user.

---
---
### function uw_campus_tour_registration_specific_date_validate($form, &$form_state)
Validates the input of the user. If there is an issue, the form will reset and an error will appear indicating the issue to the user.

---
---
### function uw_campus_tour_registration_form_validate($form, &$form_state)
Validates the input of the user. If there is an issue, the form will reset and an error will appear indicating the issue to the user.

---
---
### function uw_campus_tour_registration_date_submit($form, &$form_state)
This rebuilds the page so that the user can progress onto the next page of the form. This function is called when the submit date button on the first page is clicked by the user.

---
---
### function uw_campus_tour_registration_date_submit_phase_one($form, &$form_state)
This rebuilds the page so that the user can progress onto the next page of the form. This function is called when one of the "pick" buttons on the second page is clicked by the user.

---
---
### function uw_campus_tour_registration_redate_submit($form, &$form_state)
This rebuilds the page so that the user can progress onto the next page of the form. This function is called when the next or previous week button on the second page is clicked by the user.

---
---
### function uw_campus_tour_registration_form_thank_you($form, &$form_state)
This rebuilds the page so that the user can progress onto the next page of the form. This function is called when the submit button on the third page is clicked by the user.

---
---
### function uw_campus_tour_registration_form_submit($form, &$form_state)
Submits the form and sends the data to the CRM to be stored. It creates a lead in the CRM and creates a tour booking date.

---
---
### function uw_group_tour_registration_date_validate($form, &$form_state)
Validates the input of the user. If there is an issue, the form will reset and an error will appear indicating the issue to the user.

---
---
### function uw_group_tour_registration_form_validate($form, &$form_state)
Validates the input of the user. If there is an issue, the form will reset and an error will appear indicating the issue to the user.

---
---
### function uw_group_tour_registration_date_submit($form, &$form_state)
This rebuilds the page so that the user can progress onto the next page of the form. This function is called when the submit date button on the first page is clicked by the user.

---
---
### function uw_group_tour_registration_form_submit($form, &$form_state)
Submits the form and sends the data to the CRM to be stored. It creates a contact in the CRM and creates a tour booking date.

---
---
# Functions from autoresponse.inc

---
---
### function uw_campus_tour_registration_sendAutoResponseEmail($vname, $vdate, $vcolleges, $vtours, $vemail, $vspecialrequest, $vinternational)
This function generates the body of the email and sends it to the email address. It assumes the email is already filtered and valid.

---
---
### function uw_group_tour_registration_sendAutoResponseEmail($vname, $vdate, $vcolleges, $vtours, $vemail, $vspecialrequest, $vinternational)
This function generates the body of the email and sends it to the email address. It assumes the email is already filtered and valid. This is used for the group tour form.

---
---
### function uw_campus_tour_registration_sendAutoMail($from, $to, $message)
Generates the subject, header, from, to, and body of the email. It then mails it to the recipient.

---
---
### function uw_campus_tour_registration_createAutoMail($vname, $vdate, $vcolleges, $vtours, $vspecialrequest, $vinternational)
This replaces the placeholders with the actual values, such as first name, last name, the specific tours the user selected, the date, and etc.

##### Returns
The email template with the placeholders replaced.

---
---
### function uw_campus_tour_registration_thank_you_page($vname, $vdate, $vcolleges, $vtours, $vspecialrequest, $vinternational)
This replaces the placeholders with the actual values, such as first name, last name, the specific tours the user selected, the date, and etc. This is used for the thank you page that the user sees once they have completed the form.

##### Returns
The html template with the placeholders replaced.

---
---
### function uw_group_tour_registration_createAutoMail($vname, $vdate, $vcolleges, $vtours, $vspecialrequest, $vinternational)
This replaces the placeholders with the actual values, such as first name, last name, the specific tours the user selected, the date, and etc. This is used for the group tour form.

##### Returns
The email template with the placeholders replaced.

---
---
### function uw_campus_tour_registration_genScheduleTbl($vtours)
Generates the schedule table that shows which tours the user selected. This is used by the email generator and the thank you page.

##### Returns
An HTML schedule table that shows the tour and the time the user selected.

---
---
# Functions from crm.inc

---
---
### function uw_campus_tour_registration_crm_create_lead($session_id, $fname, $lname, $email, $state, $postal_code, $country, $admitted, $entry, $opt_out, $lead_type)
Saves the submitted data to the CRM. It adds a lead and stores data such as first name, last name, and etc.

##### Returns
The response of the server.

---
---
### function uw_campus_tour_registration_crm_create_contact($session_id, $fname, $lname, $email)
Saves the submitted data to the CRM. It adds a contact and stores data such as first name, last name, and etc.

##### Returns
The response of the server.

---
---
### function uw_campus_tour_registration_crm_authorization(&$session_id, &$url)
This authorizes access to the CRM.

---
---
### function uw_campus_tour_registration_crm_call($method, $parameters, $url)
This is the cURL request that transfer the data.

##### Returns
The response of the server.

---
---
### function uw_campus_tour_registration_get_tour_times($date, $preferences=array(), $group_size)
Gets tour times for a specific date based on preferences and group size. If $preferences is empty, it will get all tour times on the $date.

TODO: Preferences has been removed and is always empty. This function should be updated to reflect this change.

##### Returns
A list of tours for the date selected.

---
---
### function uw_campus_tour_registration_get_booked_times($booking_id)
Gets the tours that the user selected from the database. This is used for the email and thank you page.

##### Returns
The registered list of tours.

---
---
# Functions from datecheck.inc

---
---
### function uw_campus_tour_registration_form_date_past($d)
This checks if the date selected by the user has already passed.

##### Parameters
- $d: The date selected in the form of YYYY-MM-DD.

##### Returns
True if it is an earlier date than the current date, false otherwise.

---
---
### function uw_campus_tour_registration_is_valid_date($date)
Checks to see if the date is in the YYYY-MM-DD format.

##### Returns
True if the date is in the correct format, false otherwise.

---
---
### function uw_campus_tour_registration_not_weekend($validated)
Checks to see if the date selected is not on the weekend.

##### Parameters
- $validated: The date selected by the user in the form of YYYY-MM-DD.

##### Returns
False if the date is on a weekend, true otherwise.

---
---
### function uw_campus_tour_registration_not_sunday($validated)
Checks to see if the date selected is not on a Sunday.

##### Parameters
- $validated: The date selected by the user in the form of YYYY-MM-DD.

##### Returns
False if the date is on a Sunday, true otherwise.

---
---
### function uw_campus_tour_registration_not_next_two_biz_days($validated)
Checks to see if the date selected is within the date range of 2 business days. A user is not allowed to go on a tour within the next 2 business days, they may only go on tours 2 business days after the current date.

##### Parameters
- $validated: The date selected by the user in the form of YYYY-MM-DD.

##### Returns
False if the date is within the next 2 business days, true otherwise.

---
---
### function uw_campus_tour_registration_not_next_three_weeks($validated)
Checks to see if the date selected is within the date range of 3 weeks. A group tour is not allowed to go on a tour within the next 3 weeks, they may only go on a tour 3 weeks after the current date.

##### Parameters
- $validated: The date selected by the user in the form of YYYY-MM-DD.

##### Returns
False if the date is within the next 3 weeks, true otherwise.

---
---
### function uw_campus_tour_registration_first_tour_date()
This finds the very first valid tour date for the user from the current date. In general, this will be 3 days from the current date (4 days if there is a Sunday in between).

##### Returns
The first valid tour date.

---
---
# Functions from utils.inc

---
---
### function uw_campus_tour_registration_is_decimal($val)
Checks to see if the group size entered by the user is a decimal number. An integer number is the expected value.

##### Parameters
- $val: The group size value

##### Returns
True if the value is a decimal, false otherwise.

---
---
### function uw_campus_tour_registration_tick_concat($a)
Adds ticks before and after a string. This is used strictly for the CRM as this is how it expects its data.

##### Parameters
- $a: The string that needs to be ticked.

##### Returns
The string with ticks concatenated.

TODO: This function appears to be used only for preferences. If this is the case, we can remove this function as preferences has been removed.

---
---
### function uw_campus_tour_registration_calculateEndTime($t, $d)
Determines at what time the tour ends. This is used for the email and thank you page.

##### Parameters
- $t: The time of the tour in the 24 hour format.
- $d: The duration of the tour.

##### Returns
The time at which the tour ends.

---
---
### function uw_campus_tour_registration_isValidEmail($email)
Determines whether or not the provided email address is valid based on its address. This is determined through regex.

##### Returns
True if the email address appears to be valid, false otherwise.

---
---
### function uw_campus_tour_registration_convert12Hour($t)
Converts a time from the 24 hour format to the 12 hour format. As an example, it would convert 13:00 to 1:00PM.

##### Parameters
- $t: The time in the 24 hour format. 

##### Returns
The time in the 12 hour format.

---
---
### function uw_campus_tour_registration_genlabel($fieldval, $fieldtype, $duration, $mode)
Converts to a label with this format: tour name @ tour time. This is used for the email.

##### Returns
A string with the human readable label.

---
---
### function uw_campus_tour_registration_genlabel_thank_you($fieldval, $fieldtype, $duration, $mode)
Converts to a label with this format: tour name @ tour time. This is used for the thank you page.

##### Returns
A string with the human readable label.

---
---
# Functions from uw_campus_tour_registration.js

---
---
### function pick_tour(val)
Selects the tour that the user picked.

---
---
### function left()
If the user is on a mobile phone, they will only see one column of tours. They will be able to view the other tours through buttons. This function hides the current column of tours and shows the one to the left of it.

---
---
### function right()
If the user is on a mobile phone, they will only see one column of tours. They will be able to view the other tours through buttons. This function hides the current column of tours and shows the one to the right of it.

---
---
### function overlap_times_new(val, val2)
This checks to see if any of the tour times are conflicting with one another. For example, if there is a tour from 2:00PM - 3:00PM, and another tour from 2:30PM - 3:30PM, they are conflicting.

##### Returns
True or false depending on whether or not a tour conflicts with another.

---
---
### function button_conflicter(val, val2, val3)
If the user selects one tour that conflicts with another, this function disables the option of selecting the conflicted tour. It also disables the option of selecting the same tour multiple times.