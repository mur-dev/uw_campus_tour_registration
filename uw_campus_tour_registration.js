/**
 * @file
 * Javascript for the CTR
 */

(function($) {
  Drupal.behaviors.somebehaviour = {
    attach: function(context, settings) {
      $('.view_button').click(function(event) {
        var target = $(event.target);
        setTimeout(function() {
          document.getElementById(target.attr('id')).classList.add("picked");
        }, 50);
        setTimeout(function() {
          document.getElementById(target.attr('id')).value = "Loading";
          document.getElementById("myForm").submit();
        }, 50);
      });
    }
  };
})(jQuery);

jQuery (document).submit(function (e) {
    setTimeout(function() {
      var inputs = document.getElementsByTagName("INPUT");
      for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == 'submit') {
          inputs[i].disabled = true;
        }
      }
    }, 100);

    if (document.getElementById("go_right") || document.getElementById("go_left")) {
      document.getElementById("go_right").disabled = true;
      document.getElementById("go_left").disabled = true;
    }
});

function pick_tour(val) {

  var tours = ["monday_checkbox", "tuesday_checkbox", "wednesday_checkbox", "thursday_checkbox", "friday_checkbox", "saturday_checkbox"];
  for (var i = 0; i < tours.length; i++) {
    if (val == tours[i])
      document.getElementById(tours[i]).checked = true;
    else
      document.getElementById(tours[i]).checked = false;
  }
}

function left() {

  var col_1 = document.getElementsByClassName("col_1");
  var col_2 = document.getElementsByClassName("col_2");
  var col_3 = document.getElementsByClassName("col_3");
  var col_4 = document.getElementsByClassName("col_4");
  var col_5 = document.getElementsByClassName("col_5");
  var col_6 = document.getElementsByClassName("col_6");

  var col_checkbox_array = ["col1_checkbox", "col2_checkbox", "col3_checkbox", "col4_checkbox", "col5_checkbox", "col6_checkbox"];
  var col_class_checkbox_array = [col_1, col_2, col_3, col_4, col_5, col_6];

  for (var i = 0; i < col_checkbox_array.length; i++) {

    if (document.getElementById(col_checkbox_array[i]).checked == true && col_checkbox_array[i] != "col1_checkbox") {
    
      document.getElementById(col_checkbox_array[i - 1]).checked = true;
      document.getElementById(col_checkbox_array[i]).checked = false;

      for (var j = 0; j < col_class_checkbox_array[i].length; j++) {
        col_class_checkbox_array[i - 1][j].style.display = "table-cell";
        col_class_checkbox_array[i][j].style.display = "none";
      }
    }
  }
}

function right() {

  var col_1 = document.getElementsByClassName("col_1");
  var col_2 = document.getElementsByClassName("col_2");
  var col_3 = document.getElementsByClassName("col_3");
  var col_4 = document.getElementsByClassName("col_4");
  var col_5 = document.getElementsByClassName("col_5");
  var col_6 = document.getElementsByClassName("col_6");

  var col_checkbox_array = ["col6_checkbox", "col5_checkbox", "col4_checkbox", "col3_checkbox", "col2_checkbox", "col1_checkbox"];
  var col_class_checkbox_array = [col_6, col_5, col_4, col_3, col_2, col_1];

  for (var i = 0; i < col_checkbox_array.length; i++) {

    if (document.getElementById(col_checkbox_array[i]).checked == true && col_checkbox_array[i] != "col6_checkbox") {
    
      document.getElementById(col_checkbox_array[i - 1]).checked = true;
      document.getElementById(col_checkbox_array[i]).checked = false;

      for (var j = 0; j < col_class_checkbox_array[i].length; j++) {
        col_class_checkbox_array[i - 1][j].style.display = "table-cell";
        col_class_checkbox_array[i][j].style.display = "none";
      }
    }
  }
}

function overlap_times_new(val, val2) {

val_startHour = parseInt(val.substring(0, 2));
val_startMin = parseInt(val.substring(2, 4));
val_duration = parseInt(val.substring(5, 7));
val_durationHour = Math.floor(val_duration / 60);
val_durationMin = val_duration - val_durationHour * 60;
val_endHour = ((val_startMin + val_durationMin) >= 60) ? (val_startHour + 1 + val_durationHour) :
  (val_startHour + val_durationHour);
val_endMin = ((val_startMin + val_durationMin) >= 60) ? (val_startMin + val_durationMin - 60) :
  (val_startMin + val_durationMin);

val2_startHour = parseInt(val2.substring(0, 2));
val2_startMin = parseInt(val2.substring(2, 4));
val2_duration = parseInt(val2.substring(5, 7));
val2_durationHour = Math.floor(val2_duration / 60);
val2_durationMin = val2_duration - val2_durationHour * 60;
val2_endHour = ((val2_startMin + val2_durationMin) >= 60) ? (val2_startHour + 1 + val2_durationHour) :
  (val2_startHour + val2_durationHour);
val2_endMin = ((val2_startMin + val2_durationMin) >= 60) ? (val2_startMin + val2_durationMin - 60) :
  (val2_startMin + val2_durationMin);

return !((val_startHour > val2_endHour) || (val2_startHour > val_endHour) || ((val_startHour == val2_endHour) && (val_startMin >= val2_endMin)) || ((val2_startHour == val_endHour) && (val2_startMin >= val_endMin)));
}

function button_conflicter(val, val2, val3) {

    if (document.getElementById(val).classList.contains("enabled_buttons")) {

        var a_time = val.substring(val.indexOf("_") + 1);
        var a_name = val.substring(0, val.indexOf("_"));

        for (i = 0; i < val2.length; i++) {

            var b_time = val2[i].substring(val2[i].indexOf("_") + 1);
            var b_name = val2[i].substring(0, val2[i].indexOf("_"));

            if(b_name != "group"){
                if ((overlap_times_new(a_time, b_time) && a_name != b_name) || (a_name == b_name && a_time != b_time)) {

                    document.getElementById(val2[i]).classList.remove("enabled_buttons");
                    document.getElementById(val2[i]).classList.add("disabled_buttons");
                    document.getElementById(val2[i]).value = "Conflicting";
                }
            }
        }

        document.getElementById(val).classList.remove("enabled_buttons");
        document.getElementById(val).classList.add("added_buttons");
        document.getElementById(val).value = "Added";
        document.getElementById("tour_checkbox_" + val3).checked = true;
    }

    else if (document.getElementById(val).classList.contains("added_buttons")) {

        document.getElementById(val).classList.remove("added_buttons");
        document.getElementById(val).classList.add("enabled_buttons");
        document.getElementById(val).value = "Add";
        document.getElementById("tour_checkbox_" + val3).checked = false;

        var a_time = val.substring(val.indexOf("_") + 1);
        var a_name = val.substring(0, val.indexOf("_"));

        for (i = 0; i < val2.length; i++) {

            var b_time = val2[i].substring(val2[i].indexOf("_") + 1);
            var b_name = val2[i].substring(0, val2[i].indexOf("_"));

            if(b_name != "group"){
                if ((overlap_times_new(a_time, b_time) && a_name != b_name) || (a_name == b_name && a_time != b_time)) {

                    document.getElementById(val2[i]).classList.remove("disabled_buttons");
                    document.getElementById(val2[i]).classList.add("enabled_buttons");
                    document.getElementById(val2[i]).value = "Add";

                    for (j = 0; j < val2.length; j++) {

                        var c_time = val2[j].substring(val2[j].indexOf("_") + 1);
                        var c_name = val2[j].substring(0, val2[j].indexOf("_"));

                        if(c_name != "group"){
                            if ((overlap_times_new(b_time, c_time) && b_name != c_name && document.getElementById(val2[j]).classList.contains("added_buttons")) || (b_name == c_name && document.getElementById(val2[j]).classList.contains("added_buttons"))) {
                                document.getElementById(val2[i]).classList.remove("enabled_buttons");
                                document.getElementById(val2[i]).classList.add("disabled_buttons");
                                document.getElementById(val2[i]).value = "Conflicting";
                            }
                        }
                    }
                }
            }
        }
    }
}
