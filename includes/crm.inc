<?php
/**
 * @file
 * Functions involving calls to the crm.
 */
/**
 * CRM call to create a lead
 * Assume the user is already successfully authenticated
 */
function uw_campus_tour_registration_crm_create_lead($session_id, $fname, $lname, $email, $phone, $state, $country, $admitted, $entry, $opt_out, $lead_type) {
  $url = variable_get('crm_url', NULL);
  // convert opt-out field for CRM
  if ($opt_out == 1) $opt_out = 0;
  $set_entry_parameters = array(
    //session id
    'session' => $session_id,
    'module_name' => 'Leads',
    "name_value_list" => array(
      array(
        'first_name' => $fname,
        'last_name' => $lname,
        'email1' => $email,
        'phone_home' => $phone,
        'primary_address_state' => $state,
        'primary_address_country' => $country,
        'status' => $admitted,
        'admitterm_c' => $entry,
        'email_opt_out' => $opt_out,
        'campaign_id' => '8b14ddbe-fbf2-9782-ece6-546cb74fad35',
        'lead_type_c' => $lead_type,
      ),
    ),
  );
  $result = uw_campus_tour_registration_crm_call("set_entries", $set_entry_parameters, $url);
  $lead_id = $result["ids"][0];
  return $lead_id;
}
/**
 * crm call to create a contact
 * Assume the user is already successfully authenticated
 */
function uw_campus_tour_registration_crm_create_contact($session_id, $fname, $lname, $email, $phone) {
  $full_url = $_SERVER['HTTP_HOST'] . request_uri();
//  if (strpos($full_url, 'wms-dev.') === FALSE) {
    $url = variable_get('uw_campus_tour_crmlogin_url', 'https://crm-mur.uwaterloo.ca/sugarcrm/service/v4_1/rest.php');
  //} else {
    //$url = variable_get('uw_campus_tour_crmlogin_url', 'https://sugarcrm-dev.private.uwaterloo.ca/service/v4_1/rest.php');
  //}
  $set_entry_parameters = array(
    //session id
    'session' => $session_id,
    'module_name' => 'Contacts',
    "name_value_list" => array(
      array(
        'first_name' => $fname,
        'last_name' => $lname,
        'email1' => $email,
        'phone_work' => $phone,
      ),
    ),
  );
  $result = uw_campus_tour_registration_crm_call("set_entries", $set_entry_parameters, $url);
  $lead_id = $result["ids"][0];
  return $lead_id;
}
/**
 * log into the crm
 */
function uw_campus_tour_registration_crm_authorization(&$session_id, &$url) {
  $url = variable_get('crm_url', NULL);
  $username = variable_get('crm_user', NULL);
  $password = variable_get('crm_pass', NULL);
  //login ------------------------------------------------
  $login_parameters = array(
    "user_auth" => array(
      "user_name" => $username,
      "password" => md5($password),
      "version" => "1"
      ),
    "application_name" => "RestTest",
    "name_value_list" => array(),
  );
  $login_result = uw_campus_tour_registration_crm_call("login", $login_parameters, $url);
  if(!empty($login_result)) {
    //get session id
    $session_id = $login_result['id'];
  } else {
    $session_id = NULL;
  }
  
}
/**
 * Function to make cURL request
 * Returns a JSON reponse in an array
 */
function uw_campus_tour_registration_crm_call($method, $parameters, $url) {
  ob_start();
  $curl_request = curl_init();
  curl_setopt($curl_request, CURLOPT_URL, $url);
  curl_setopt($curl_request, CURLOPT_POST, 1);
  curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
  curl_setopt($curl_request, CURLOPT_HEADER, 1);
  curl_setopt($curl_request, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
  $json_encoded_data = json_encode($parameters);
  $post = array(
    "method" => $method,
    "input_type" => "JSON",
    "response_type" => "JSON",
    "rest_data" => $json_encoded_data
  );
  curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
  $result = curl_exec($curl_request);
  curl_close($curl_request);

  if ( ! empty($result)) {
    $result = explode("\r\n\r\n", $result, 2);

    $response = json_decode($result[1], TRUE);
  } else {
    $response = [];
  }
  ob_end_flush();
  return $response;
}
/**
 * Gets tour times based on preferences
 * If $preferences is empty, it will get all tour times on the $date
 */
function uw_campus_tour_registration_get_tour_times($date, $preferences=array(), $group_size) {
  uw_campus_tour_registration_crm_authorization($session_id, $url);
  // Create date object for selected tour date
  $date_obj = date_create($date);
  // Get day-of-week for selected tour date
  $visit_day = lcfirst(date_format($date_obj, 'l'));
  //create array for mapping user preferences to crm options
  $filter = array();
  $tour_types = array(
    "interest_Campus" => "campus_tour",
    "interest_v1" => "v1_tour",
    "interest_rev" => "rev_tour",
    "interest_uwp" => "uwp_tour",
    "interest_ahs" => "ahs_tour",
    "interest_arts" => "arts_tour",
    "interest_afm" => "afm_tour",
    "interest_Engineering" => "eng_tour",
    "interest_env" => "env_tour",
    "interest_Math" => "math_tour",
    "interest_cs" => "cs_tour",
    "interest_Science" => "sci_tour",
    "interest_colleges_conrad" => "grebel",
    "interest_colleges_renison" => "renison",
    "interest_colleges_stjeromes" => "sju",
    "interest_colleges_stpauls" => "stp",
    "interest_colleges_all" => "uc_tour",
    "group" => "group",
  );
  //map crm tour types to crm tour times fields
  $tour_time_field = array(
    "campus_tour" => "campus_tour_time",
    "v1_tour" => "v1_tour_time",
    "rev_tour" => "rev_tour_time",
    "uwp_tour" => "uwp_tour_time",
    "ahs_tour" => "ahs_tour_time",
    "arts_tour" => "art_tour_time",
    "afm_tour" => "afm_tour_time",
    "eng_tour" => "eng_tour_time",
    "env_tour" => "env_tour_time",
    "math_tour" => "mat_tour_time",
    "cs_tour" => "cs_tour_time",
    "sci_tour" => "sci_tour_time",
    "grebel" => "gre_tour_time",
    "renison" => "ren_tour_time",
    "sju" => "stj_tour_time",
    "stp" => "stp_tour_time",
    "alluc" => "alluc_tour_time_c",
    "group" => "group_tour_time_c",
  );
  //create the SQL filter based on the number of preferences
  foreach ($preferences as $p) {
    //exception for rev and uwp
    if ($p == "interest_v1") {
        $filter[] = " tour_type_c= 'rev_tour' OR tour_type_c = 'v1_tour' ";
    }
    elseif ($p == "interest_uwp") {
        $filter[] = " tour_type_c= 'uwp_tour' OR tour_type_c = 'campus_tour' ";
    }
    else {
        $filter[] = " tour_type_c =  '" . $tour_types[$p] . "'";
    }
    //$filter[] = ($p == "interest_uwp") ? " tour_type_c= 'uwp_tour' OR tour_type_c = 'campus_tour' " :" tour_type_c =  '" . $tour_types[$p] . "'";
  }
  $sql_filter = implode(" OR ", $filter);
  // If there are no preferences indicated, only limit by date (show all options)
  if (empty($sql_filter)) {
    $sql_filter = " start_date <= '" . $date . "' AND end_date >= '" . $date . "'";
  }
  // Otherwise, filter by preferences for the date specified
  else {
    $sql_filter = "( " . $sql_filter . " ) ";
    $sql_filter .= " AND (start_date <= '" . $date . "' AND end_date >= '" . $date . "')";
  }
  //get tour times from tour schedule module
  $tour_schedules = array();
  $get_entries_list_parameters = array(
    'session' => $session_id,
    'module_name' => 'ctreg_ctr_schedule',
    'query' => $sql_filter,
    'order_by' => "form_sort_order_c asc",
    'offset' => 0,
    'select_fields' => array(
      'id',
      'name',
      $visit_day . '_1',
      $visit_day . '_2',
      $visit_day . '_3',
      $visit_day . '_cap_c',
      'assigned_user_id',
      'assigned_user_name',
      'tour_duration',
      'tour_type_c',
    ),
    'link_name_to_fields_array' => array(),
    'max_results' => 100,
    'deleted' => 0,
    'favorites' => FALSE,
  );
  $schedule_result = uw_campus_tour_registration_crm_call("get_entry_list", $get_entries_list_parameters, $url);
  if (! empty($schedule_result)) {
    foreach ($schedule_result["entry_list"] as $item) {
      array_push($tour_schedules, $item["name_value_list"]);
    }
  }
  //get current tour enrollment
  $sql_filter = " visit_date ='" . $date . "'";
  $get_enrollment_list_parameters = array(
    'session' => $session_id,
    'module_name' => 'ctreg_ctr_bookings',
    'query' => $sql_filter,
    'order_by' => "",
    'offset' => 0,
    'select_fields' => array(
      'group_size',
      'campus_tour_time',
      'v1_tour_time',
      'rev_tour_time',
      'uwp_tour_time',
      'ahs_tour_time',
      'art_tour_time',
      'afm_tour_time',
      'eng_tour_time',
      'env_tour_time',
      'mat_tour_time',
      'cs_tour_time',
      'sci_tour_time',
      'gre_tour_time',
      'ren_tour_time',
      'stj_tour_time',
      'stp_tour_time',
      'alluc_tour_time_c',
      'group_tour_time_c',
    ),
    'link_name_to_fields_array' => array(),
    'max_results' => 100,
    'deleted' => 0,
    'favorites' => FALSE,
  );
  $enrollment_result = uw_campus_tour_registration_crm_call("get_entry_list", $get_enrollment_list_parameters, $url);
  //add the excluded and capped times
  $exceptiontimes = array();
  $cnt = count($tour_schedules);
  //get exceptions
  for ($i = 0; $i < $cnt; $i++) {
    $get_relationship_parameters = array(
      'session' => $session_id,
      'module_name' => 'ctreg_ctr_schedule',
      //The ID of the specified module bean.
      'module_id' => $tour_schedules[$i]["id"]["value"],
      'link_field_name' => 'ctreg_ctr_schedule_exception_ctreg_ctr_schedule',
      //The portion of the WHERE clause from the SQL statement used to find the related items.
      'related_module_query' => " exception_date = '" . $date . "' ",
      //The related fields to be returned.
      'related_fields' => array(
        'id',
        'name',
        'no_tour_1',
        'no_tour_2',
        'no_tour_3',
        'exception_date'
      ),
      //For every related bean returned, specify link field names to field information.
      'related_module_link_name_to_fields_array' => array(
      ),
      //To exclude deleted records
      'deleted' => 0,
      //order by
      'order_by' => '',
      //offset
      'offset' => 0,
      //limit
      'limit' => 200,
    );
    $exception_result = uw_campus_tour_registration_crm_call("get_relationships", $get_relationship_parameters, $url);
    // Process any exceptions
    if (!empty($exception_result["entry_list"])) {
      foreach ($exception_result["entry_list"] as $entry) {
        if ($entry["name_value_list"]["no_tour_1"]["value"] == "1") {
          $tour_schedules[$i][$visit_day . '_1']["value"] = "";
        }
        if ($entry["name_value_list"]["no_tour_2"]["value"] == "1") {
          $tour_schedules[$i][$visit_day . '_2']["value"] = "";
        }
        if ($entry["name_value_list"]["no_tour_3"]["value"] == "1") {
          $tour_schedules[$i][$visit_day . '_3']["value"] = "";
        }
      }
    }
    // If a cap exists, check scheduled tours for existing bookings
    if ($tour_schedules[$i][$visit_day . '_cap_c']["value"] != '') {
      for ($j = 1; $j <= 3; $j++) {
        // For the given index, proceed only if a scheduled time is set
        if (!empty($tour_schedules[$i][$visit_day . '_' . $j]['value'])) {
            $sql_filter = " visit_date ='" . $date . "' AND " . $tour_time_field[$tour_schedules[$i]["tour_type_c"]["value"]] . " = '" . $tour_schedules[$i][$visit_day . '_' . $j]["value"] . "'";
            $get_enrollment_list_parameters = array(
              'session' => $session_id,
              'module_name' => 'ctreg_ctr_bookings',
              'query' => $sql_filter,
              'order_by' => "",
              'offset' => 0,
              'select_fields' => array(
                'group_size',
              ),
              'link_name_to_fields_array' => array(),
              'max_results' => 100,
              'deleted' => 0,
              'favorites' => FALSE,
              );
            $enrollment_result=uw_campus_tour_registration_crm_call("get_entry_list", $get_enrollment_list_parameters, $url);
            $total_group_size = 0;
            foreach ($enrollment_result["entry_list"] as $entry) {
              // Add booking's group size to other groups
              $total_group_size = $total_group_size + intval($entry["name_value_list"]["group_size"]["value"]);
            }
            // If total group size + requested group size exceeds scheduled cap, remove option from list
            if (($total_group_size + $group_size) > $tour_schedules[$i][$visit_day . '_cap_c']["value"]) {
              $tour_schedules[$i][$visit_day . '_' . $j]["value"] = "";
            }
        }
      }
    }
  }
  $tour_type_ids = array(
    "campus_tour" => "Campus",
    "v1_tour" => "Village",
    "rev_tour" => "Ron",
    "uwp_tour" => "uwp",
    "ahs_tour" => "ahs",
    "arts_tour" => "arts",
    "afm_tour" => "AFM",
    "eng_tour" => "Engineering",
    "env_tour" => "Environment",
    "math_tour" => "Math",
    "sci_tour" => "Science",
    "grebel" => "Grebel",
    "renison" => "Renison",
    "sju" => "sju",
    "stp" => "stp",
    "uc_tour" => "alluc",
    "group" => "group",
  );
  //add output to array
  $opt_list = array();
  for ($i = 0; $i < $cnt; $i++) {
    // Create the checklist items for the form
    if ($tour_schedules[$i][$visit_day . '_1']["value"]) {
      $str = uw_campus_tour_registration_genlabel($tour_schedules[$i][$visit_day . '_1']["value"], $tour_schedules[$i]["tour_type_c"]["value"], $tour_schedules[$i]["tour_duration"]["value"], "form");
      $name_arr = explode(" ", $tour_schedules[$i]["name"]["value"]);
      $val = $tour_type_ids[$tour_schedules[$i]["tour_type_c"]["value"]] . "_" . $tour_schedules[$i][$visit_day . '_1']["value"] . "_" . $tour_schedules[$i]["tour_duration"]["value"];
      $opt_list[$val] = $str;
    }
    if ($tour_schedules[$i][$visit_day . '_2']["value"]) {
      $str=uw_campus_tour_registration_genlabel($tour_schedules[$i][$visit_day . '_2']["value"], $tour_schedules[$i]["tour_type_c"]["value"], $tour_schedules[$i]["tour_duration"]["value"], "form");
      $name_arr=explode(" ", $tour_schedules[$i]["name"]["value"]);
      $val=$tour_type_ids[$tour_schedules[$i]["tour_type_c"]["value"]] . "_" . $tour_schedules[$i][$visit_day . '_2']["value"] . "_" . $tour_schedules[$i]["tour_duration"]["value"];
      $opt_list[$val]=$str;
    }
    if ($tour_schedules[$i][$visit_day . '_3']["value"]) {
      $str = uw_campus_tour_registration_genlabel($tour_schedules[$i][$visit_day . '_3']["value"], $tour_schedules[$i]["tour_type_c"]["value"], $tour_schedules[$i]["tour_duration"]["value"], "form");
      $name_arr = explode(" ", $tour_schedules[$i]["name"]["value"]);
      $val = $tour_type_ids[$tour_schedules[$i]["tour_type_c"]["value"]] . "_" . $tour_schedules[$i][$visit_day . '_3']["value"] . "_" . $tour_schedules[$i]["tour_duration"]["value"];
      $opt_list[$val] = $str;
    }
  }
  return $opt_list;
}
/**
 * Get tour times of a tour booking
 */
function uw_campus_tour_registration_get_booked_times($booking_id) {
  uw_campus_tour_registration_crm_authorization($session_id, $url);
  //get info from module
  $get_entry_parameters = array(
    //session id
    "session" => $session_id,
    //The name of the module from which to retrieve records.
    "module_name" => "ctreg_ctr_bookings",
    'query' => "ctreg_ctr_bookings.id = '$booking_id'",
    'order_by' => "form_sort_order_c asc",
    'offset' => 0,
    'link_name_to_fields_array' => array(
      'campus_tour_time' => 'campus_tour_time',
      'v1_tour_time' => 'v1_tour_time',
      'rev_tour_time' => 'rev_tour_time',
      'uwp_tour_time' => 'uwp_tour_time',
      'ahs_tour_time' => 'ahs_tour_time',
      'art_tour_time' => 'art_tour_time',
      'eng_tour_time' => 'eng_tour_time',
      'env_tour_time' => 'env_tour_time',
      'mat_tour_time' => 'mat_tour_time',
      'sci_tour_time' => 'sci_tour_time',
      'afm_tour_time' => 'afm_tour_time',
      'gre_tour_time' => 'gre_tour_time',
      'ren_tour_time' => 'ren_tour_time',
      'stj_tour_time' => 'stj_tour_time',
      'stp_tour_time' => 'stp_tour_time',
      'alluc_tour_time_c' => 'alluc_tour_time_c',
      'group_tour_time_c' => 'group_tour_time_c',
    ),
  'max_results' => 2,
  'deleted' => 0,
  'favorites' => FALSE,
  );
  $result = uw_campus_tour_registration_crm_call("get_entry_list", $get_entry_parameters, $url);
  //there should only be no more than 1 array returned
  $times = $result["entry_list"][0]["name_value_list"];
  //loop through each time and insert them in order
  $fields = array('campus_tour_time', 'v1_tour_time', 'rev_tour_time', 'uwp_tour_time', 'ahs_tour_time', 'art_tour_time', 'eng_tour_time', 'env_tour_time', 'mat_tour_time', 'afm_tour_time', 'sci_tour_time', 'gre_tour_time', 'ren_tour_time', 'stj_tour_time', 'stp_tour_time', 'alluc_tour_time_c', 'group_tour_time_c');
  $displaycodes = array(
    'campus_tour_time' => 'Campus tour',
    'v1_tour_time' => 'Campus &amp; residence tour',
    'rev_tour_time' => 'Ron Eydt Village tour',
    'uwp_tour_time' => 'UW Place residence',
    'ahs_tour_time' => 'AHS faculty tour',
    'art_tour_time' => 'Arts faculty tour',
    'eng_tour_time' => 'Engineering faculty tour',
    'env_tour_time' => 'Environment faculty tour',
    'mat_tour_time' => 'Mathematics and Computer Science tour',
    'sci_tour_time' => 'Science faculty tour',
    'afm_tour_time' => 'AFM tour',
    'gre_tour_time' => 'Conrad Grebel tour',
    'ren_tour_time' => 'Renison tour',
    'stj_tour_time' => "St. Jerome's tour",
    'stp_tour_time' => "St. Paul's tour",
    'alluc_tour_time_c' => "Joint UC tour",
    'group_tour_time_c' => 'Group tour',
  );
  $registered = array();
  foreach ($fields as $f) { //loop through each field and determine if it is set
    if ($times[$f]["value"]) {
      $index = intval($times[$f]["value"]);
      $registered[$index] = $displaycodes[$f]; //assume there are no conflicting tours so we can set the start times as the index
    }
  }
  return $registered;
}
