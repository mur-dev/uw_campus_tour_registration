<?php

/**
 * @file
 * Misc helper functions.
 */

/**
 * Function returns true if value has decimals(checks for whole numbers)
 */
function uw_campus_tour_registration_is_decimal($val) {
  return is_numeric( $val ) && floor( $val ) != $val;
}

/**
 * Adds ticks before and after a string
 */
function uw_campus_tour_registration_tick_concat($a) {
  return "^" . $a . "^";
}

/**
 * Function for calculating tour end time
 * d is in the format of 12:00, 15:00, 8:00
 */
function uw_campus_tour_registration_calculateEndTime($t, $d) {
  $time = strtotime($t);
  $calculation = "+" . $d . " minutes";
  return date("H:i", strtotime($calculation, $time));
}

/**
 * Returns whether or not an email is valid based on it's address
 * @author    Brad Galway <bgalway@uwaterloo.ca>
 * @param   $email email address
 * @return     BOOL
 */
function uw_campus_tour_registration_isValidEmail($email) {
  if (preg_match('/^[_A-z0-9-]+((\.|\+)[_A-z0-9-]+)*@[A-z0-9-]+(\.[A-z0-9-]+)*(\.[A-z]{2,4})$/', $email)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Function for conversion to 12 hour time
 * input is HH:MM
 */
function uw_campus_tour_registration_convert12Hour($t) {
  $t_arr = explode(":", $t);
  $hour = intval($t_arr[0]);
  if (($hour >= 0) && ($hour < 12)) {
    $meridian = "am";
  }
  elseif ($hour == 12) {
    $meridian = "pm";
  }
  else {
    $meridian = "pm";
    $hour -= 12;
  }
  return strval($hour) . ":" . $t_arr[1] . " " . $meridian;
}

/**
 * Function for conversion to tour @ time format for checklist
 * Input is time, tour type
 */
function uw_campus_tour_registration_genlabel($fieldval, $fieldtype, $duration, $mode) {
  if ($mode == "form") {
    //display names for tour type
    $tour_type_labels = array(
      "campus_tour" => "Campus tour",
      "v1_tour" => "Campus &amp; residence tour",
      "rev_tour" => "Ron Eydt Village",
      "uwp_tour" => "UW Place tour",
      "ahs_tour" => "Applied Health Science faculty tour",
      "arts_tour" => "Arts faculty tour",
      "afm_tour" => "Accounting and Financial Management tour",
      "eng_tour" => "Engineering faculty tour",
      "env_tour" => "Environment faculty tour",
      "math_tour" => "Mathematics and Computer Science tour",
      "sci_tour" => "Science faculty tour",
      "grebel" => "Grebel tour",
      "renison" => "Renison tour",
      "sju" => "St. Jerome's tour",
      "stp" => "St. Paul's tour",
      "uc_tour" => "Joint UC tour",
      "group" => "Group tour"
    );
  }
  else {
    $tour_type_labels = array(
      "Village" => "Campus &amp; residence tour",
      "Ron" => "Ron Eydt Village tour",
      "uwp" => "UW Place tour",
      "ahs" => "Applied Health Science faculty tour",
      "arts" => "Arts faculty tour",
      "AFM" => "Accounting and Financial Management tour",
      "Engineering" => "Engineering faculty tour",
      "Environment" => "Environment faculty tour",
      "Math" => "Mathematics and Computer Science tour",
      "Science" => "Science faculty tour",
      "Campus" => "Campus tour",
      "Grebel" => "Grebel tour",
      "Renison" => "Renison tour",
      "sju" => "St. Jerome's tour",
      "stp" => "St. Paul's tour",
      "alluc" => "Joint UC tour",
      "group" => "Group tour",
    );
  }

  $converted = substr_replace($fieldval, ":", 2, 0);
  $end_time = uw_campus_tour_registration_calculateEndTime($converted, $duration);
  $displayname = $tour_type_labels[$fieldtype];

  return $displayname . " @ " . uw_campus_tour_registration_convert12Hour($converted) . " - " . uw_campus_tour_registration_convert12Hour($end_time);
}

/**
 * Function for conversion to tour @ time format for checklist
 * Input is time, tour type
 */
function uw_campus_tour_registration_genlabel_thank_you($fieldval, $fieldtype, $duration, $mode) {

  //display names for tour type
  $tour_type_labels = array(
    "Village" => "Campus &amp; residence tour",
    "Ron" => "Ron Eydt Village tour",
    "uwp" => "UW Place tour",
    "ahs" => "Applied Health Science faculty tour",
    "arts" => "Arts faculty tour",
    "AFM" => "Accounting and Financial Management tour",
    "Engineering" => "Engineering faculty tour",
    "Environment" => "Environment faculty tour",
    "Math" => "Mathematics and Computer Science tour",
    "Science" => "Science faculty tour",
    "Campus" => "Campus tour",
    "Grebel" => "Grebel tour",
    "Renison" => "Renison tour",
    "sju" => "St. Jerome's tour",
    "stp" => "St. Paul's tour",
    "alluc" => "Joint UC tour",
    "group" => "Group tour",
  );

  $converted = substr_replace($fieldval, ":", 2, 0);
  $end_time = uw_campus_tour_registration_calculateEndTime($converted, $duration);
  $displayname = $tour_type_labels[$fieldtype];

  return $displayname . " @ " . uw_campus_tour_registration_convert12Hour($converted) . " - " . uw_campus_tour_registration_convert12Hour($end_time);
}