<?php

/**
 * @file
 * Functions for date validation.
 */

/**
 * Checks if $d is in the past
 * Returns true if it is an earlier date than today, false otherwise
 * $d is in the form of YYYY-MM-DD
 */
function uw_campus_tour_registration_form_date_past($d) {
  $date = date_create_from_format('Y-m-d', $d);
  $today = date_create();
  return $date < $today;
}

/**
 * Checks if date is a valid date in YYYY-MM-DD format
 */
function uw_campus_tour_registration_is_valid_date($date) {
  if (!is_string($date)) {
    return FALSE;
  }
  $format = "Y-m-d";
  $formatted_date = date_create_from_format($format, $date);
  return (!$formatted_date) ? FALSE : TRUE;
}

/**
 * Checks if date is not on a weekend
 * Assume input to function is a valid YYYY-MM-DD string
 */
function uw_campus_tour_registration_not_weekend($validated) {
  $date_day = date('N', strtotime($validated));
  if ($date_day == 7 || $date_day == 6) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Checks if date is not on a sunday
 * Assume input to function is a valid YYYY-MM-DD string
 */
function uw_campus_tour_registration_not_sunday($validated) {
  $date_day = date('N', strtotime($validated));
  if ($date_day == 7) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Check if a date is within date range
 * 2 business days
 * Assume the input to the function is already a valid date object
 */
function uw_campus_tour_registration_not_next_two_biz_days($validated) {
  $date = strtotime($validated);
  $date_day = date('N', $date);
  $date_date = date('Y-m-d', $date);
  $today = date('Y-m-d');
  $today_day = date('N');
  $p1date = date('Y-m-d', strtotime("+ 1 day"));
  $p2date = date('Y-m-d', strtotime("+ 2 days"));
  $p3date = date('Y-m-d', strtotime("+ 3 days"));
  $p4date = date('Y-m-d', strtotime("+ 4 days"));

  //check if today is Thursday
  if ($today_day == 4) {
    if ($date_date == $p1date || $date_date == $p2date || $date_date == $p4date || $date_date == $today) {
      return FALSE;
    }
  }

  //check if today is a Friday
  elseif ($today_day == 5) {
    if ($date_date == $p1date || $date_date == $p3date || $date_date == $p4date || $date_date == $today) {
      return FALSE;
    }
  }

  //check if today a Saturday
  elseif ($today_day == 6) {
    if ($date_date == $p2date || $date_date == $p3date || $date_date == $today) {
      return FALSE;
    }
  }
  else {
    if ($date_date == $p2date || $date_date == $p1date || $date_date == $today) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Check if a date is within date range
 * 2 weeks
 * Assume the input to the function is already a valid date object
 */
function uw_campus_tour_registration_not_next_three_weeks($validated) {
  $date = strtotime($validated);
  $date_date = date('Y-m-d', $date);
  $min_date = date('Y-m-d', strtotime("+ 21 days"));

  if ($date_date <= $min_date) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Returns the first avaliable tour date from current date
 */
function uw_campus_tour_registration_first_tour_date() {
  $today_date = date("N");

  if ($today_date == 4 || $today_date == 5) {
    return date("Y-m-d", time() + (5 * 24 * 60 * 60));
  }
  elseif ($today_date == 6) {
    return date("Y-m-d", time() + (4 * 24 * 60 * 60));
  }
  else {
    return date("Y-m-d", time() + (3 * 24 * 60 * 60));
  }
}