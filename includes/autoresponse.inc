<?php
/**
 * @files
 * Functions to aid the sending of the form's auto-response message.
 */

/**
 * wrapper function to generate body and send email
 * assume email is already filtered and valid
 */
function uw_campus_tour_registration_sendAutoResponseEmail($vname, $vdate, $vcolleges, $vtours, $vemail, $vspecialrequest, $vinternational) {
  $fromaddr = variable_get('uw_campus_tour_email_sender', 'vcinfo@uwaterloo.ca');
  $fromname = variable_get('uw_campus_tour_email_name', 'University of Waterloo Visitors Centre');
  $from = $fromname . " <" . $fromaddr . "> ";
  $to = $vemail;
  $message = uw_campus_tour_registration_createAutoMail($vname, $vdate, $vcolleges, $vtours, $vspecialrequest, $vinternational);
  uw_campus_tour_registration_sendAutoMail($from, $to, $message);
}

/**
 * wrapper function to generate body and send email
 * assume email is already filtered and valid
 */
function uw_group_tour_registration_sendAutoResponseEmail($vname, $vdate, $vcolleges, $vtours, $vemail, $vspecialrequest, $vinternational) {
  $fromaddr = variable_get('uw_campus_tour_email_sender', 'vcinfo@uwaterloo.ca');
  $fromname = variable_get('uw_campus_tour_email_name', 'University of Waterloo Visitors Centre');
  $from = $fromname . " <" . $fromaddr . "> ";
  $to = $vemail;
  $message = uw_group_tour_registration_createAutoMail($vname, $vdate, $vcolleges, $vtours, $vspecialrequest, $vinternational);
  uw_campus_tour_registration_sendAutoMail($from, $to, $message);
}

/**
 * Generates From and To and sends email
 */
function uw_campus_tour_registration_sendAutoMail($from, $to, $message) {
  $subject = variable_get('uw_campus_tour_autoresponse_subject', 'Thank you for booking a campus tour!');
  $subject = str_ireplace(array("\r", "\n", '%0A', '%0D'), '', $subject); //prevent email injections
  $headers = "MIME-Version: 1.0" . "\r\n" . "Content-type:text/html;charset=UTF-8" . "\r\n" . "From: " . $from . "\r\n" . "Reply-To: " . $from . "\r\n" . 'X-Mailer: PHP/' . phpversion();
  $body = str_replace("\n.", "\n..", $message); //prevent email injections
  mail($to, $subject, $message, $headers);
}

/**
 * Replaces placeholders with values
 */
function uw_campus_tour_registration_createAutoMail($vname, $vdate, $vcolleges, $vtours, $vspecialrequest, $vinternational) {
  $path = drupal_get_path('module', 'uw_campus_tour_registration');
  $tmparr = variable_get('uw_campus_tour_autoresponse_body', array('value' => '', 'format' => NULL));
  $template = $tmparr["value"];
  $vcemail = variable_get('uw_campus_tour_email_vcemail', 'vcinfo@uwaterloo.ca');
  $email_vars = array('vname', 'vdate', 'vcemail', 'vspecialrequest', 'vinternational');

  //replace placeholders
  foreach ($email_vars as $key) {
    $template = str_replace('{{ ' . $key . ' }}', $$key, $template);
  }

  //replace vtours with tour schedule table
  //assume $vtours is an array
  //assume that $vtours is an array with the index as the time and tour type as the value
  $vtourtable = uw_campus_tour_registration_genScheduleTbl($vtours);
  $template = str_replace('{{ vtours }}', $vtourtable, $template);

  /*
  //replace colleges, if selected
  if (empty($vcolleges)) {
    $template = str_replace('{{ vcolleges }}', "" , $template);
  }
  else {
    $colleges_text = "<p>You have indicated an interest in the following university colleges. A representative from these institutions will contact you to schedule a time to visit while you're on campus.</p><p>";
    $colleges_text .= $vcolleges;
    $colleges_text .= "</p>";
    $template = str_replace('{{ vcolleges }}', $colleges_text , $template);
  }*/

  return $template;
}

/**
 * Replaces placeholders with values
 */
function uw_campus_tour_registration_thank_you_page($vname, $vdate, $vcolleges, $vtours, $vspecialrequest, $vinternational) {
  $path = drupal_get_path('module', 'uw_campus_tour_registration');
  $tmparr = variable_get('uw_campus_tour_thank_you_page', array('value' => '', 'format' => NULL));
  $template = $tmparr["value"];
  $vcemail = variable_get('uw_campus_tour_email_vcemail', 'vcinfo@uwaterloo.ca');
  $email_vars = array('vname', 'vdate', 'vcemail', 'vspecialrequest', 'vinternational');

  //replace placeholders
  foreach ($email_vars as $key) {
    $template = str_replace('{{ ' . $key . ' }}', $$key, $template);
  }

  return $template;
}

/**
 * Replaces placeholders with values
 */
function uw_group_tour_registration_createAutoMail($vname, $vdate, $vcolleges, $vtours, $vspecialrequest, $vinternational) {
  $path = drupal_get_path('module', 'uw_campus_tour_registration');
  $tmparr = variable_get('uw_group_tour_autoresponse_body', array('value' => '', 'format' => NULL));
  $template = $tmparr["value"];
  $vcemail = variable_get('uw_campus_tour_email_vcemail', 'vcinfo@uwaterloo.ca');
  $email_vars = array('vname', 'vdate', 'vcemail', 'vspecialrequest', 'vinternational');

  //replace placeholders
  foreach ($email_vars as $key) {
    $template = str_replace('{{ ' . $key . ' }}', $$key, $template);
  }

  //replace vtours with tour schedule table
  //assume $vtours is an array
  //assume that $vtours is an array with the index as the time and tour type as the value
  $vtourtable = uw_campus_tour_registration_genScheduleTbl($vtours);
  $template = str_replace('{{ vtours }}', $vtourtable, $template);

  /*
  //replace colleges, if selected
  if (empty($vcolleges)) {
    $template = str_replace('{{ vcolleges }}', "" , $template);
  }
  else {
    $colleges_text = "<p>You have indicated an interest in the following university colleges. A representative from these institutions will contact you to schedule a time to visit while you're on campus.</p><p>";
    $colleges_text .= $vcolleges;
    $colleges_text .= "</p>";
    $template = str_replace('{{ vcolleges }}', $colleges_text , $template);
  }*/

  return $template;
}

/**
 * Generate the schedule table
 * Assume that $vtours is an array with the index as the time and tour type as the value
 */
function uw_campus_tour_registration_genScheduleTbl($vtours) {
  $sortedvtours = $vtours;
  ksort($sortedvtours);
  //create the table headers;
  $table = "<table cellpadding=3 cellspacing=0 style='width:500px;'>";
  $table .= "<thead><tr>";
  $table .= "<td style='background:#000;color:#fff;font-weight:bold;width:200px;line-height:1.5;border-right:1px solid #a7a9ac;border-bottom:1px soild #a7a9ac;'>TIME</td>";
  $table .= "<td style='background:#000;color:#fff;font-weight:bold;line-height:1.5;border-bottom:1px soild #a7a9ac;'>ACTIVITY</td>";
  $table .= "</tr></thead>\n<tbody>";
  $i = 0;
  foreach ($sortedvtours as $key => $value) {
    $i++;
    if ($key < 1000) { //append 0 in front, if smaller than 1000, necessary for 12 hour conversion to work
      $formatted_time = substr_replace($key, ":", 1, 0); //insert colon in time
      $formatted_time = "0" . $formatted_time;
    }
    else {
      $formatted_time = substr_replace($key, ":", 2, 0); //insert colon in time
    }
    $formatted_time = uw_campus_tour_registration_convert12Hour($formatted_time);

    if ($i % 2 == 0) {
      $table .= "<tr>";
      $table .= "<td style='line-height:1.5;background:#ddd;border-right:1px solid #a7a9ac;border-bottom:1px soild #a7a9ac;'>$formatted_time</td>\n";
      $table .= "<td style='line-height:1.5;background:#ddd;border-bottom:1px soild #a7a9ac;'>$value</td>\n";
      $table .= "</tr>";
    }
    else {
      $table .= "<tr>";
      $table .= "<td style='line-height:1.5;border-right:1px solid #a7a9ac;border-bottom:1px soild #a7a9ac;'>$formatted_time</td>\n";
      $table .= "<td style='line-height:1.5;border-bottom:1px soild #a7a9ac;'>$value</td>\n";
      $table .= "</tr>";
    }
  }

  $table .= "</tbody></table>";
  return $table;
}